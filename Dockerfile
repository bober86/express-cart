### STAGE 1: Build
# Docker Parent Image with Node and Typescript
FROM node:alpine as builder

# Create Directory for the Container
WORKDIR /app

RUN npm install typescript -g

# Copy the files we need to our new Directory
ADD . /app

RUN rm -rf node_modules
RUN npm cache clean --force

# Grab dependencies and transpile src directory to dist
RUN npm install && npm install -D && npm run build

RUN ls
RUN pwd

### STAGE 2: serve the app
FROM node:alpine

WORKDIR /app

ENV NODE_ENV="production"
ENV SESSION_SECRET="Z7vVufbj9Bl57qrEUPMgTkn4ZreJ4kO2L3BasZVodQ2SfvCueBr2L4avw3amWdnzo5qdZVqVUPZcwVSGax"


RUN npm install -g nodemon

COPY --from=builder /app/dist /app/dist
COPY --from=builder /app/package.json /app/
COPY --from=builder /app/package-lock.json /app/

RUN npm install
EXPOSE 4000
# Start the server
# ENTRYPOINT ["node", "dist/"]
CMD ["nodemon", "dist/server.js"]
