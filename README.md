# express-cart
Important  
All price are in 1/100 of the main unit ex. price 100 is 1 dollar, 50 is 0.5 dollar


### Requirement

- [Node.js v12.14.1](https://nodejs.org/en/)
- [Typescript 3.7+](https://www.typescriptlang.org/index.html#download-links)
- [Docker](https://docs.docker.com/engine/installation/) (Optional)
- [Docker Compose](https://docs.docker.com/compose/) (Optional)


### Start the project

```bash
$ npm i
$ npm run build
$ npm run start
```

If you don't want install any dependency on your computer you can use docker to run project

```bash
$ docker-compose up
```
Remember to set correctly variables in docker-compose.yml file

```$xslt
    environment:
      SESSION_SECRET: "12331332"
```
### Documentation
Documentation (swagger) is available on url:
```
http://localhost:4000/docs
```
### Npm Scripts

- Use `npm run lint` to check code style
- Use `npm test` to run unit test
- Use `npm run build` to compiled project from ts to js
