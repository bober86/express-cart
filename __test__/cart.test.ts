import Cart from '../src/utils/cart';

describe('Cart class', function () {
    const product = {
        'id': '2',
        'description': 'Fruits',
        'quantity': 1,
        'name': 'Oranges',
        'price': {
            'value': 500,
            'currency': 'EUR'
        }
    };

    it('Get empty cart', async () => {
        const cart =  new Cart();
        expect(cart).toBeInstanceOf(Cart);
    });
    it('add product cart', async () => {

        const cart =  new Cart().add(product);
        expect(cart).toBeInstanceOf(Cart);
        expect(cart.getCart().totalItems).toBe(1);
        expect(cart.getCart().items['2'].product).toBe(product);
        expect(cart.getCart().items['2'].totalQuantity).toBe(1);

        cart.add(product);
        // add again this same product we need have total quantity 2
        expect(cart.getCart().items['2'].totalQuantity).toBe(2);
    });

    it('remove product cart', async () => {
        const cart =  new Cart().add(product);
        cart.remove(product.id);
        expect(cart.getCart().totalItems).toBe(0);
        expect(cart.getCart().items).toStrictEqual({});

    });

    it('checkout cart', async () => {
        const cart =  await new Cart().add(product).checkout('PLN');

        expect(cart.getCart().totalPrice.value).toBeGreaterThan(0);
        expect(cart.getCart().totalPrice.currency).toBe('PLN');

    });
});
