import request from 'supertest';
import app from '../src/app';
import { Product } from '../src/interfaces/product';

export function post(app: any, url: string, body: Product){
    const httpRequest = app.post(url);
    httpRequest.send(body);
    httpRequest.set('Accept', 'application/json');
    httpRequest.set('Origin', 'http://localhost:4000');
    return httpRequest;
}


const bodyPostProduct = {
    'id': '2',
    'description': 'Fruits',
    'quantity': 1,
    'name': 'Oranges',
    'price': {
        'value': 500,
        'currency': 'EUR'
    }
};
let cookie: string;
let responseBody: string;
describe('GET /api/cart', function () {
    it('Get empty cart', async () => {
        const response =  await request(app).get('/api/cart');
        expect(response.status).toBe(200);
        expect(response.body.status).toBe('success');
        expect(response.body.result).toStrictEqual({});
    });
});

describe('POST /api/cart/product', () => {

    it('add product to cart', async () => {

        const server = await request(app);
        const response =  await post(server,'/api/cart/product', bodyPostProduct);
        cookie = response.headers['set-cookie'].pop().split(';')[0];
        responseBody = response.body;
        expect(response.status).toBe(200);
        expect(response.body.status).toBe('success');
        expect(response.body.result.items['2'].product).toStrictEqual(bodyPostProduct);
        expect(response.body.result.totalItems).toBe(1);


    });

    it('get cart', async () => {
        const response =  request(app).get('/api/cart');
        response.cookies = cookie;
        const result = await response.set('Accept','application/json');
        expect(result.status).toBe(200);
        expect(result.body.status).toBe('success');
        expect(result.body).toStrictEqual(responseBody);
    });
});

describe('DELETE /api/cart/product', () => {
    it('get cart before delete', async () => {
        const response =  request(app).get('/api/cart');
        response.cookies = cookie;
        const result = await response.set('Accept','application/json');
        expect(result.body).toStrictEqual(responseBody);
    });
    it('delete cart', async () => {
        const response =  request(app).delete('/api/cart');
        response.cookies = cookie;
        const result = await response.set('Accept','application/json');
        expect(result.body).toStrictEqual('');
        expect(result.status).toBe(200);
    });
    it('get cart after delete', async () => {
        const response =  request(app).get('/api/cart');
        response.cookies = cookie;
        const result = await response.set('Accept','application/json');
        expect(result.body.status).toBe('success');
        expect(result.body.result).toStrictEqual({});
    });
});
