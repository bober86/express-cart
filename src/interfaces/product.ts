export interface Product {
    id: string;
    name: string;
    price: Price;
    quantity: number;
    description: string;
}

export interface Price {
    value: number;
    currency: string;
}
