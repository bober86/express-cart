import request from 'request';
import { Response } from 'request';

interface CurrencyI {
    rates: RatesI;
    base: string;
    date: string;
}
interface RatesI {
    [index: string]: number;
}
export class Currency {
    protected endpoint = 'https://api.exchangeratesapi.io/latest';
    protected baseCurr: string;
    protected currencies: CurrencyI;

    constructor(unit: string) {
        this.baseCurr = unit;
    }

    public sendRequest(): Promise<CurrencyI> {
        const reqOptions: request.Options = {
            url: this.endpoint,
            method: 'GET',
            qs: {base: this.baseCurr}
        };
        return new Promise((resolve, reject) => {
            request.get(reqOptions, (err: any, response: Response, body: string) => {
                if (err || response.statusCode !== 200) {
                    return reject(err);
                }
                try {
                    return resolve(JSON.parse(body));
                } catch (e) {
                    return reject(`Error when parsing string to JSON: ${e} `);
                }

            });
        });
    }
    public async getData() {
        this.currencies = await this.sendRequest();
    }
    public get(currUnit: string) {
        return this.currencies.rates[currUnit];
    }


}
