import logger from './logger';
import dotenv from 'dotenv';
import fs from 'fs';

if (fs.existsSync('.env')) {
    logger.debug('Using .env file to supply config environment variables');
    dotenv.config({ path: '.env' });
} else {
    logger.debug('Using .env.example file to supply config environment variables');
    dotenv.config({ path: '.env.example' }); // remove this file and create own .env
}

export const ENVIRONMENT = process.env.NODE_ENV;
export const MONGO_URL = process.env['MONGODB_URI'];
export const SESSION_SECRET = process.env['SESSION_SECRET'];
