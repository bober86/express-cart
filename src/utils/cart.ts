import { Price, Product } from '../interfaces/product';
import { Currency } from './currency';

interface Items {
    [index: string]: ProductItem;
}

interface ProductItem {
    product: Product;
    totalQuantity: number;
    totalPrice: Price;
}

class Cart {
    protected items: Items = {};
    protected totalItems = 0;
    protected totalPrice: Price;

    constructor(cart = { items: {}, totalItems: 0 }) {
        this.items = cart.items ? cart.items : {};
        this.totalItems = cart.totalItems ? cart.totalItems : 0;
    }

    public add(item: Product) {
        if (this.items[item.id]) {
            this.items[item.id].totalQuantity += item.quantity;
            this.items[item.id].totalPrice.value = item.price.value * this.items[item.id].totalQuantity;
            return this;
        } else {
            this.items[item.id] = {
                product: item,
                totalQuantity: item.quantity,
                totalPrice: {
                    value: item.price.value * item.quantity,
                    currency: item.price.currency,
                },
            };
            this.totalItems++;
            return this;
        }
    }

    public remove(productId: string) {
        if (this.items[productId]) {
            this.totalItems--;
            delete this.items[productId];
        }
    }
    public async checkout(currUnit: string = 'EUR'){
        const currConverter = new Currency(currUnit);
        await currConverter.getData();

        const result = Object.values(this.items).reduce((acc, product: ProductItem) => {
                const unit = product.totalPrice.currency;
                const convertUnit = currConverter.get(unit);
                return acc + (product.totalPrice.value / convertUnit);
            },
            0);
        this.totalPrice = {
            value: +result.toFixed(),
            currency: currUnit,
        };
        return this;
    }

    getCart() {
        return { items: this.items, totalItems: this.totalItems, totalPrice: this.totalPrice };
    }
}

export default Cart;
