import Joi from '@hapi/joi';

import { Request, Response, NextFunction } from 'express';
import { createResponse, statusType } from '../utils/response';
import logger from '../utils/logger';

const validator = (joiSchema: Joi.Schema, property: 'body' | 'query'  = 'body') => {
    return (req: Request, res: Response, next: NextFunction) => {
        const { error } = joiSchema.validate(req[property]);
        const valid = error == null;

        if (valid) {
            next();
        } else {
            const { details } = error;
            const message = details.map(i => i.message).join(',');

            logger.error(message);
            res.status(422).json(createResponse(statusType.error, message));
        }
    };
};
export default validator;
