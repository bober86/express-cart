import Joi from '@hapi/joi';

const price = Joi.object().keys({
    value: Joi.number().required(),
    currency: Joi.string().required()
}).required();

const schemaProduct = Joi.object().keys({
    id: Joi.string().required(),
    description: Joi.string().optional(),
    quantity: Joi.number().required(),
    name: Joi.string().required(),
    price: price,
});

export default schemaProduct;
