import Joi from '@hapi/joi';
const currency = [
    'CAD', 'HKD', 'ISK', 'PHP', 'DKK', 'HUF', 'CZK', 'GBP', 'RON', 'SEK',
    'IDR', 'INR', 'BRL', 'RUB', 'HRK', 'JPY', 'THB', 'CHF', 'EUR', 'MYR',
    'TRY', 'CNY', 'NOK', 'NZD', 'ZAR', 'USD', 'MXN', 'SGD', 'AUD', 'ILS',
    'KRW', 'PLN'
];
export const schemaCheckout = Joi.object({
    currency: Joi.string().required().valid(...currency),
});
