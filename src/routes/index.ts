import express from 'express';
import cartRoutes from './cart';
import productRoutes from './product';
const router = express.Router();

router.use('/cart', cartRoutes);
router.use('/product', productRoutes);

export default router;
