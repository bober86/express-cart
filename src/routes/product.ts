import express from 'express';
import fs from 'fs';
import path from 'path';
import { Response, Request } from 'express';
import { createResponse, statusType } from '../utils/response';

const jsonPath = path.join(__dirname, '..', 'products.json');

const products = JSON.parse(fs.readFileSync(jsonPath,'utf8'));

const productRoutes = express.Router();

productRoutes.get('/',
    (req: Request, res: Response) => {

        res.json(createResponse(statusType.success, products));
    });

export default productRoutes;
