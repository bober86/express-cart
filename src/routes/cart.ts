import express from 'express';
import { Response, Request } from 'express';

import { createResponse, statusType } from '../utils/response';
import Cart from '../utils/cart';
import schemaProduct from '../schemas/product';
import validator from '../middleware/validator';
import { schemaCheckout } from '../schemas/checkout';


const cartRoutes = express.Router();
cartRoutes.get('/', (req: Request, res: Response) => {

    const cart = req.session.cart ? req.session.cart : {};
    req.session.cart = cart;

    res.json(createResponse(statusType.success, cart));
});
cartRoutes.delete('/', (req: Request, res: Response) => {

    delete req.session.cart;
    res.status(200).json();
});
cartRoutes.post('/product', validator(schemaProduct),(req: Request, res: Response) => {
    const cart = new Cart(req.session.cart ? req.session.cart : {});
    cart.add(req.body);
    req.session.cart = cart;
    res.json(createResponse(statusType.success, cart));
});

cartRoutes.delete('/product/:productId', (req: Request, res: Response) => {
    const {productId} = req.params;
    const cart = new Cart(req.session.cart ? req.session.cart : {});
    cart.remove(productId);
    req.session.cart = cart;
    res.json(cart);
});
cartRoutes.get('/checkout', validator(schemaCheckout, 'query'), async (req: Request, res: Response) => {
    const { currency } = req.query;
    const cart = new Cart(req.session.cart ? req.session.cart : {});
    await cart.checkout(currency);
    req.session.cart = cart;
    res.json(createResponse(statusType.success, cart));
});

export default cartRoutes;
