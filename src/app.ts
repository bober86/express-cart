import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { Response, Request } from 'express';
import compression from 'compression';
import helmet from 'helmet';
import session from 'express-session';
import lusca from 'lusca';
import swaggerUi from 'swagger-ui-express';

import { createResponse, statusType } from './utils/response';
import mainRoutes from './routes/index';
import { SESSION_SECRET } from './utils/secrets';

import * as swaggerDocument from './swagger.json';

const app = express();

app.use(compression());
app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: SESSION_SECRET,
}));

app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.set('port', process.env.PORT || 4000);

app.get('/', (req: Request, res: Response) => res.json(createResponse(statusType.success, 'Api is working')));
app.use('/api', mainRoutes);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

export default app;

